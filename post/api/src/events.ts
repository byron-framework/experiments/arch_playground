import { PostData } from "./data";

export class BrokerEvent {
  type: string;
  payload: any;
  topic: string;

  constructor(type: string, payload: any, topic: string) {
    this.type = type;
    this.payload = payload;
    this.topic = topic;
  }

  getTopic(): string {
    return this.topic;
  }

  serialize() {
    const copy = JSON.parse(JSON.stringify(this));
    delete copy.topic;
    return JSON.stringify(copy);
  }
}

export class NewPostBrokerEvent extends BrokerEvent {
  constructor(post: PostData) {
    super("NewPost", post._id, `new.post`);
  }
}

export class PostTitleUpdatedBrokerEvent extends BrokerEvent {
  constructor(post: PostData) {
    super("PostTitleUpdated", post.title, `post.${post._id}`);
  }
}

export class PostBodyUpdatedBrokerEvent extends BrokerEvent {
  constructor(post: PostData) {
    super("PostBodyUpdated", post.body, `post.${post._id}`);
  }
}

export class PostUserUpdatedBrokerEvent extends BrokerEvent {
  constructor(post: PostData) {
    super("PostUser_IdUpdated", post.user_id, `post.${post._id}`);
  }
}

export class PostUpdatedBrokerEvent extends BrokerEvent {
  constructor(post: PostData) {
    super("PostUpdated", post, `post.${post._id}`);
  }
}