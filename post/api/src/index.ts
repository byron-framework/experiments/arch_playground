import { GraphQLServer } from "graphql-yoga";
import "reflect-metadata";
import { buildSchema } from "type-graphql";
import { MongoClient } from "mongodb";

import PostResolver from "./resolvers/PostResolver";

const PORT: string = (process.env.PORT as string);
const MONGO_URL: string = (process.env.MONGO_URL as string);
const MONGO_DB_NAME: string = (process.env.MONGO_DB_NAME as string);

MongoClient.connect(MONGO_URL, async (err: any, db: any) => {
  if (err) throw err;

  const schema = await buildSchema({
    resolvers: [ PostResolver ],
    emitSchemaFile: true
  });

  const context = {
    cache: db.db(MONGO_DB_NAME)
  };

  const server = new GraphQLServer({
    schema,
    context
  });

  server.start({ port: PORT }, ({ port }) => {
    console.log(`Server is running on http://localhost:${port}`);
  });
});


