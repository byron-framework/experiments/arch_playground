import * as STAN from "node-nats-streaming";

import { PostData } from "./data";
import {
  BrokerEvent,
  NewPostBrokerEvent,
  PostTitleUpdatedBrokerEvent,
  PostBodyUpdatedBrokerEvent,
  PostUserUpdatedBrokerEvent,
  PostUpdatedBrokerEvent
} from "./events";

const BROKER_ID: string = (process.env.BROKER_ID as string);
const BROKER_CLIENT_ID: string = (process.env.BROKER_CLIENT_ID as string);
const BROKER_URL: string = (process.env.BROKER_URL as string);

class Publisher {
  broker: any;
  events: BrokerEvent[];

  constructor() {
    this.events = [];
    this.broker = STAN
      .connect(BROKER_ID, BROKER_CLIENT_ID, { url: BROKER_URL });
  }

  pubCB(err: any, guid: any) {
    if (err) console.log(err);
    else console.log(guid);
  }
}

class FineGrainedPostPublisher extends Publisher {
  constructor() {
    super();
  }

  async publish(post: PostData) {
    this.events.push(new NewPostBrokerEvent(post));
    this.events.push(new PostBodyUpdatedBrokerEvent(post));
    this.events.push(new PostTitleUpdatedBrokerEvent(post));
    this.events.push(new PostUserUpdatedBrokerEvent(post));

    while (this.events.length) {
      const e: BrokerEvent = (this.events.shift() as BrokerEvent);
      this.broker.publish(e.getTopic(), e.serialize(), this.pubCB);
      console.log(`[FINISHED] ${e.payload}`);
    }
  }
}

class CoarseGrainedPostPublisher extends Publisher {
  constructor() {
    super();
  }

  async publish(post: PostData) {
    this.events.push(new PostUpdatedBrokerEvent(post));

    while (this.events.length) {
      const e: BrokerEvent = (this.events.shift() as BrokerEvent);
      await this.broker.publish(e.getTopic(), e.serialize(), this.pubCB);
      console.log(`[FINISHED] ${e.payload}`);
    }
  }
}

export class PostEventPublisherSingleton {
  private static _instance: PostEventPublisherSingleton;
  publisher: FineGrainedPostPublisher | CoarseGrainedPostPublisher;

  private constructor() {
    const fineGrained = (new Boolean(process.env.FINE_GRAINED) as boolean);

    if (fineGrained)
      this.publisher = new FineGrainedPostPublisher();
    else
      this.publisher = new CoarseGrainedPostPublisher();
  }

  public static getInstance() {
    return this._instance || (this._instance = new this());
  }

  async publish(post: PostData) {
    await this.publisher.publish(post);
  }
}