export interface UserData {
  _id: number;
}

export interface PostData {
  _id: number;
  title: string;
  body: string;
  user_id: number;
}

export interface Context {
  cache: any;
  broker: any;
}
