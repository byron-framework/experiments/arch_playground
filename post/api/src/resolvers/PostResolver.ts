import { Query, Mutation, Resolver, Arg, Ctx, FieldResolver, Root } from "type-graphql";
import { Context, UserData, PostData } from "../data";
import Post from "../schemas/Post";
import { PostEventPublisherSingleton } from "../eventsProcessor";

@Resolver(of => Post)
export default class {
  @Query(returns => [Post])
  async getPosts(@Ctx() ctx: Context): Promise<PostData[]> {
    let posts: PostData[] = await ctx.cache.collection('posts').find({}).toArray();
    // console.log(posts);
    return posts;
  }

  @Mutation(returns => Post)
  async createPost(
    @Arg("title") title: string,
    @Arg("body") body: string,
    @Arg("user_id") user_id: number,
    @Ctx() ctx: Context): Promise<PostData> {

    if (title == "")
      throw new Error("Cannot create post with empty title");

    if (body == "")
      throw new Error("Cannot create post with empty body");

    let n: number = await ctx.cache.collection('posts').countDocuments({});

    let id: number = n + 1;

    let post: PostData =  {
      _id: id,
      title,
      user_id,
      body
    };

    const p = PostEventPublisherSingleton.getInstance();
    await p.publish(post);

    return post;
  }

  @FieldResolver()
  async user(@Root() postData: PostData, @Ctx() ctx: Context) {
    let user: UserData = await ctx.cache
      .collection('users').findOne({ _id: postData.user_id });
    
    return user;
  }
}
