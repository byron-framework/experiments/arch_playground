import { Field, Int, ObjectType } from "type-graphql";
import User from "./User";

@ObjectType()
export default class Post {
  @Field(type => Int)
  _id: number;

  @Field()
  title: string;

  @Field()
  body: string;

  @Field(type => User)
  user: User;
}
