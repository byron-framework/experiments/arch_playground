import { Field, Int, ObjectType } from "type-graphql";
import Post from "./Post";

@ObjectType()
export default class User {
  @Field(type => Int)
  _id: number;

  @Field(type => [Post])
  posts: Post[];
}
