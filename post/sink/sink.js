"use strict";

const {
  BROKER_ID,
  BROKER_CLIENT_ID,
  BROKER_URL,
  MONGO_URL,
  MONGO_DB_NAME
} = process.env;

const stan = require('node-nats-streaming')
  .connect(BROKER_ID, BROKER_CLIENT_ID, { url: BROKER_URL });

const optReplayAll = stan
  .subscriptionOptions()
  .setDeliverAllAvailable();

const MongoClient = require('mongodb').MongoClient;

const cache = new MongoClient(MONGO_URL);

const postIDTopicHandler = async (msg) => {
  const { type, payload } = JSON.parse(msg.getData()),
    attr = type.replace(/(Post|Updated)/g, '').toLowerCase(),
    topic = msg.getSubject(),
    id = Number.parseInt(topic.replace('post.',''));

  try {
    const db = cache.db(`cache`);

    let x, obj = {};
    obj[attr] = payload;

    x = await db.collection('posts').updateOne(
      { _id: { $eq: id } },
      { $set: obj }
    );

    // console.log(x);

  } catch(err) {
    console.log(err.stack);
  }
};

stan.on('connect', async () => {
  try {
    await cache.connect();

    const subs = {
      newUser: stan.subscribe('new.user', optReplayAll),
      newPost: stan.subscribe('new.post', optReplayAll)
    };

    subs.newUser.on("message", async (msg) => {
      const id = Number.parseInt(msg.getData());

      try {
        const db = cache.db(MONGO_DB_NAME);

        let x = await db
          .collection('users')
          .insertOne(
            { _id: id },
            { forceServerObjectId: true }
          );
        // console.log(x);
      } catch (err) {
        console.log(err.stack);
      }

    });

    subs.newPost.on("message", async (msg) => {
      const id = Number.parseInt(msg.getData()),
        topic = `post.${id}`;

      try {
        const db = cache.db(MONGO_DB_NAME);

        let x = await db
          .collection('posts')
          .insertOne(
            { _id: id },
            { forceServerObjectId: true }
          );

        subs[topic] = stan.subscribe(topic, optReplayAll);
        subs[topic].on("message", postIDTopicHandler);
      } catch (err) {
        console.log(err.stack);
      }
    });
  } catch (err) {
    console.log(err.stack);
  }
});
