export interface UserData {
  _id: number;
  name: string;
  email: string;
}

export interface Context {
  cache: any;
  broker: any;
}
