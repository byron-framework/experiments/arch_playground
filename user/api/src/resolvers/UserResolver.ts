import { Query, Mutation, Resolver, Arg, Ctx } from "type-graphql";
import { Context, UserData } from "../data";
import User from "../schemas/User";

const pubCB = (err: any, guid: any) => {
  if (err) console.log(err);
  // else console.log(`Published ${guid}`);
};

@Resolver(of => User)
export default class {
  @Query(returns => [User])
  async getUsers(@Ctx() ctx: Context): Promise<UserData[]> {
    let x: User[] = await ctx.cache.collection('users').find({}).toArray();
    // console.log(x);

    return x;
  }

  @Mutation(returns => User)
  async createUser(@Arg("name") name: string, @Arg("email") email: string, @Ctx() ctx: Context): Promise<UserData> {
    if (name == "")
      throw new Error("Cannot create user with empty name");

    if (email == "")
      throw new Error("Cannot create user with empty email");


    let n: number = await ctx.cache.collection('users').countDocuments({});

    let id = n+1;

    ctx.broker.publish(`new.user`, id.toString(), pubCB);

    let msg: string = JSON.stringify({
      type: "UserEmailUpdated",
      payload: email
    });
    ctx.broker.publish(`user.${id}`, msg, pubCB);

    msg = JSON.stringify({
      type: "UserNameUpdated",
      payload: name
    });
    ctx.broker.publish(`user.${id}`, msg, pubCB);

    return {
      _id: id,
      name,
      email
    };
  }
}
