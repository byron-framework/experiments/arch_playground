import { GraphQLServer } from "graphql-yoga";
import "reflect-metadata";
import { buildSchema } from "type-graphql";
import { MongoClient } from "mongodb";
import * as STAN from "node-nats-streaming";

import UserResolver from "./resolvers/UserResolver";

let broker: any;

const PORT: string = (process.env.PORT as string);
const MONGO_URL: string = (process.env.MONGO_URL as string);
const MONGO_DB_NAME: string = (process.env.MONGO_DB_NAME as string);
const BROKER_ID: string = (process.env.BROKER_ID as string);
const BROKER_CLIENT_ID: string = (process.env.BROKER_CLIENT_ID as string);
const BROKER_URL: string = (process.env.BROKER_URL as string);

MongoClient.connect(MONGO_URL, async (err: any, db: any) => {
  if (err) throw err;

  const schema = await buildSchema({
    resolvers: [ UserResolver ],
    emitSchemaFile: true
  });

  broker = STAN 
    .connect(BROKER_ID, BROKER_CLIENT_ID, { url: BROKER_URL });

  const context = {
    cache: db.db(MONGO_DB_NAME),
    broker
  };

  const server = new GraphQLServer({
    schema,
    context
  });

  server.start({ port: PORT }, ({ port }) => {
    console.log(`Server is running on http://localhost:${port}`);
  });
});


