"use strict";

const {
  BROKER_ID,
  BROKER_CLIENT_ID,
  BROKER_URL,
  MONGO_URL,
  MONGO_DB_NAME
} = process.env;

const stan = require('node-nats-streaming')
  .connect(BROKER_ID, BROKER_CLIENT_ID, { url: BROKER_URL });

const optReplayAll = stan
  .subscriptionOptions()
  .setDeliverAllAvailable();

const MongoClient = require('mongodb').MongoClient;

const cache = new MongoClient(MONGO_URL);

const userIDTopicHandler = async (msg) => {
  const { type, payload } = JSON.parse(msg.getData()),
    attr = type.replace(/(User|Updated)/g, '').toLowerCase(),
    topic = msg.getSubject(),
    id = Number.parseInt(topic.replace('user.',''));

  try {
    const db = cache.db(MONGO_DB_NAME);

    let x;

    if (attr == "email") {

      x = await db.collection('users').updateOne(
        { _id: { $eq: id } },
        { $set: { email: payload } }
      );
    }
    else {
      x = await db.collection('users').updateOne(
        { _id: { $eq: id } },
        { $set: { name: payload } }
      );
    }

  } catch(err) {
    console.log(err.stack);
  }
};

stan.on('connect', async () => {
  try {
    await cache.connect();

    const subs = {
      newUser: stan.subscribe('new.user', optReplayAll)
    };

    subs.newUser.on("message", async (msg) => {
      const id = Number.parseInt(msg.getData()),
        topic = `user.${id}`;

      try {
        const db = cache.db(MONGO_DB_NAME);

        let x = await db
          .collection('users')
          .insertOne(
            {
              _id: id,
            },
            { forceServerObjectId: true }
          );

        subs[topic] = stan.subscribe(topic, optReplayAll);
        subs[topic].on('message', userIDTopicHandler);
      } catch (err) {
        console.log(err.stack);
      }

    });
  } catch (err) {
    console.log(err.stack);
  }
});
